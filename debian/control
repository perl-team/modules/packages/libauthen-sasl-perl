Source: libauthen-sasl-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libdigest-hmac-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libauthen-sasl-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libauthen-sasl-perl.git
Homepage: https://metacpan.org/release/Authen-SASL
Rules-Requires-Root: no

Package: libauthen-sasl-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends}
Suggests: libdigest-hmac-perl,
          libgssapi-perl
Description: Authen::SASL - SASL Authentication framework
 SASL is a generic mechanism for authentication used by several network
 protocols. Authen::SASL provides an implementation framework that all
 protocols should be able to share.
 .
 The framework allows different implementations of the connection class
 to be plugged in. At the time of writing there were two such plugins
 in Debian:
  - Authen::SASL::Perl (in this package)
    This module implements several mechanisms and is implemented
    entirely in Perl.
  - Authen::SASL::XS (in the libauthen-sasl-xs-perl package)
    This module uses the Cyrus SASL C-library (both version 1 and 2 are
    supported).
